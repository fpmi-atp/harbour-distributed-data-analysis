## Apache Kafka
Apache Kafka is a distributed streaming platform that uses Zookeeper under the hood.

### Architecture

### Terminology and features
* Logs are the basic units of representation in Kafka
	* _how are logs different from simple text lines, what is the key feature of storing them?_
* Message delivery semantics (by default) - At most once in case of asynchronous replication and At least once in case of synchronous.
	* _Which message delivery semantics are you familiar with? What kind of problems is each of them suited for?_
* Asynchronous replication
* Does not store the order of messages (unline regular queues)

Replication modes:
1. __Synchronous.__ Newly-delivered data is replicated synchronously on several nodes.
    * Example: Hadoop
2. __Asynchronous.__ Newly-delivered data is considered committed to the system when it has been written to at least 1 node (called a **leader**). Replication to other nodes happens after that. For different partitions, *different nodes* may be leaders.
	* Ecample: Kafka

Asynchronous replication works faster, but the probability of data loss is higher.

Some difference exists between the concepts of leader in Kafka and master (NameNode) in Hadoop:
* A master controls the execution of tasks by worker nodes, but does not make calculations for them directly.
* A leader has the functionality of a worker node, but possesses some specific properties (e.g. stores the most recent version of the data).
* In Kafka, the message is read from the leader, other nodes are only used for backup.

### Kafka CLI

Services that use Kafka:

| **Service** | **URL** |
|:-------:|:---:|
|Zookeeper servers|mipt-master.atp-fivt.org:2181|
||mipt-node01.atp-fivt.org:2181|
||mipt-node02.atp-fivt.org:2181|
|Kafka brokers|mipt-node04.atp-fivt.org:9092|
||mipt-node05.atp-fivt.org:9092|
||mipt-node06.atp-fivt.org:9092|
||mipt-node07.atp-fivt.org:9092|
||mipt-node08.atp-fivt.org:9092|
|Kafka bootstrap-servers| same as broker addresses|

#### Working with topics
To work with topics, you need to connect to Kafka. To do this, you need to pass at least one Zookeeper node as a parameter. The command contacts Kafka, gets all the Zookeeper servers and uses them to search for the desired topic.

##### Print the list of topics
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --list
```
##### Create a topic
You need to define:
* number of partitions,
* replication factor,
* topic name.
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --create \
    --partitions 6 \
    --replication-factor 2 \
    --topic for20210XX-topic
```

(write the last 2 digits of your username instead of `XX`)
Call `list` again and check that our topic has been created.

Now let's create another topic with replication factor equal to 12. We get an error, however:
```
Exception: replication factor: 12 larger than available brokers: 5
```

In Kafka, the replication factor is limited by the number of brokers (unlike Hadoop, where it's only limited by the common sense).

To delete a topic, use the appropriate argument `--delete`.
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --delete --topic for20210XX-topic
```

##### Print info about topic
The necessary argument is `--describe`
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --describe --topic for20210XX-topic
```
Output:
```
Topic:for20210XX_topic	PartitionCount:6	ReplicationFactor:2	Configs:
	Topic: for20210XX_topic	Partition: 0	Leader: 135	Replicas: 135,131	Isr: 135,131
	Topic: for20210XX_topic	Partition: 1	Leader: 131	Replicas: 131,132	Isr: 131,132
	Topic: for20210XX_topic	Partition: 2	Leader: 132	Replicas: 132,133	Isr: 132,133
	Topic: for20210XX_topic	Partition: 3	Leader: 133	Replicas: 133,134	Isr: 133,134
	Topic: for20210XX_topic	Partition: 4	Leader: 134	Replicas: 134,135	Isr: 134,135
	Topic: for20210XX_topic	Partition: 5	Leader: 135	Replicas: 135,132	Isr: 135,132
```
* 6 partitions,
* 2 replicas of each one,
* the leader and workers for each partition (e.g. 0th and 5th partitions have the same leader and different workers)

Leaders are selected using [Round-robin](https://en.wikipedia.org/wiki/Round-robin_scheduling). At the same time, Kafka is able to balance the workload depending on the current node usage.

#### Reading from and writing to a topic

* a consumer reads data using zookeeper
* a producer writes data using kafka-broker

Kafka can read data from various sources (HDFS, application outputs, databases, Amazon AWS storage) and mix them. We'll consider the simplest option - terminal.

##### Try to read data from topic
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic for20210XX-topic
```

The command won't finish, since it reads the data in realtime (=> waits for something to be written into the topic), but the topic is empty. Stop it.

##### Write something to topic
We don't need a Zookeeper server for that, only a list of Kafka brokers (one will be enough).
```
kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic for20210XX-topic
```

Let's write several consecutive numbers and stop the process.

It's possible to write the entire sequence with one command: `seq 20 | kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic for20210XX-topic`

##### Realtime read and write
Run `tmux` and create 2 panes. Run consumer in one, producer in the other.

![IMAGE ALT TEXT HERE](images/Kafka1.png)

Write the numbers and observe that they are displayed in the consumer synchronously.

We could also write to one topic from several sources and get a primitive version of a TCP chat.

Stop the processes.

##### Kafka doesn't store data delivery timestamps
Try to read the data again and see that the output is once again empty.

By default, consumer only prints new messages, i.e. those with the most recent offsets (recall offsets in Hadoop). Let's try to print the whole topic.
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic for20210XX-topic --from-beginning
```

The messages are printed, but out of order.

We can also print the data from a specific partition:
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic for20210XX-topic --from-beginning --partition 0
```

Doesn't work.  
To use that feature, we need to use new style consumers. Broadly speaking, they differ from ordinary consumers in that they work with bootstrap servers instead of brokers. That's a more general interface in Kafka which knows about both brokers and Zookeeper servers.
```
kafka-console-consumer --bootstrap-server mipt-node06.atp-fivt.org:9092 --topic for20210XX-topic --new-consumer --from-beginning --partition 0
```
Within one partition, the result is ordered.

##### User-defined scripts for Kafka.
* [Java Doc about Kafka API](https://kafka.apache.org/10/javadoc/?org/apache/kafka/),
* [Python API](https://kafka-python.readthedocs.io/en/master/usage.html) is available as well.

```
kafka-run-class kafka.tools.GetOffsetShell --broker-list  mipt-node05.atp-fivt.org:9092 --topic for20210XX-topic --time -1
```

### Integrating Spark Streaming and Kafka

The objective is to build the WordCount of a text written into topic.

##### Write a handler

Create contexts
```python
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

sc = SparkContext(master='yarn')
ssc = StreamingContext(sc, batchDuration=10)
````

Connect to topic and create a DStream (see previous class):
```python
dstream = KafkaUtils.createDirectStream(
    ssc, topics=['test'],
    kafkaParams = {'metadata.broker.list': 'mipt-node06.atp-fivt.org:9092,mipt-node07.atp-fivt.org:9092'}
)
```
Ideally, specify several brokers in case one of them crashes.

WordCount logic:
```python
result = dstream \
        .flatMap(lambda (x, line): line.split(" ")) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda x, y: x + y)
result.pprint()
```

Run the handler and wait for SIGINT (^C).

```
ssc.start()
ssc.awaitTermination()
```

##### Testing

1. Open the topic for writing.
2. In another tmux pane (or another terminal), launch our Spark app.
3. Write the text to topic and watch the output.

```
-------------------------------------------
Time: 2019-04-10 21:43:50
-------------------------------------------
(u'', 1)
(u'rh', 1)
(u'etrthrhtt', 1)
```

#### Add the Stateful approach to our program

For this purpose, we need to:
* In `updateStateByKey()` , write the state update logic.
* In `foreachRDD()`, write the result output logic.
* Don't forget about `ssc.checkpoint()`, it's required to save the state after update.

See [previous class](12-realtime.md) for more.

#### Enable the application to stop automatically when the data is depleted

1) Before processing, check whether the data is still incoming.

```python
finished = False

def set_ending_flag(rdd):
    global finished
    if rdd.isEmpty():
        finished = True

dstream.foreachRDD(set_ending_flag)
```

2) Instead of calling `ssc.awaitTermination()`, stop the context when the input ends.

```python
ssc.start()
while not finished:
    pass
ssc.stop()
```

#### Sources

`/home/velkerr/seminars/hobod2019/17-kafka`
