#### Count non-blank lines. Source = netcat.

1. Run tmux with two panes (or just two terminals).

* In one window, run `nc -lk 127.0.0.1 -p <PORT>`. Select a unique non-occupied port.
* In the other, supply the code below to `spark-submit`.

```python
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

sc = SparkContext(master='local[4]')  # local mode
ssc = StreamingContext(sc, batchDuration=10)  # form a batch from the input text every 10 seconds
dstream = ssc.socketTextStream(hostname='localhost', port=9999) # connect to port 9999 (or another on which your netcat is running)

result = dstream.filter(bool).count()  # count non-blank lines
result.pprint()  # print the result for each batch

ssc.start()
ssc.awaitTermination() # wait for SIGINT (^C)
```

2. In the terminal with netcat, type several text lines, some of them blank.
3. Watch how their count appears in the other terminal.

```
-------------------------------------------
Time: 2019-04-04 11:13:00
-------------------------------------------
6
```
6 is the number of non-blank lines.

#### Count non-blank lines. Source = HDFS.

Configure HDFS CLI:

Create `~/.hdfscli.cfg` with these contents

```
[global]
default.alias = default

[default.alias]
url = http://sber-master.atp-fivt.org:9870
user = <my_user>
```

Now write an application.

```python
import os
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext

from hdfs import Config
import subprocess

client = Config().get_client()
nn_address = subprocess.check_output('hdfs getconf -confKey dfs.namenode.http-address', shell=True).strip().decode("utf-8")
sc = SparkContext(master='yarn-client')  # since we're working with HDFS, it's better to use the distributed mode

# simulate real-life data that comes periodically in parts
DATA_PATH = "/data/realtime/en_articles_batches"
batches = [sc.textFile(os.path.join(*[nn_address, DATA_PATH, path])) for path in client.list(DATA_PATH)]  # form batches from the dataset files
BATCH_TIMEOUT = 5 # every 5s, send a batch as an RDD
ssc = StreamingContext(sc, BATCH_TIMEOUT)

dstream = ssc.queueStream(rdds=batches)
result = dstream.filter(bool).count()
result.pprint()

ssc.start()
ssc.awaitTermination()  # wait for SIGINT (^C)
```

Result:
```
-------------------------------------------
Time: 2019-04-04 11:20:14
-------------------------------------------
4256
```

When the whole dataset has been processed, we'll start seeing 0 in results and can kill the app.

#### WordCount

```
def print_rdd(rdd):
    for row in rdd.take(10):
        print('{}\t{}'.format(*row))

result = dstream \
    .flatMap(lambda line: line.split()) \
    .map(lambda word: (word, 1)) \
    .reduceByKey(lambda x, y: x + y) \
    .foreachRDD(lambda rdd: print_rdd(rdd.sortBy(lambda x: -x[1])))
```

Wait for the results to end and kill the app again.

What are the drawbacks of our current solution?

* The results are not accumulated
* It's inconvenient to kill the application manually

## Stateful

### UpdateStateByKey Operation
The updateStateByKey operation allows you to maintain arbitrary state while continuously updating it with new information. To use this, you will have to do two steps.

1. Define the state - The state can be an arbitrary data type.
2. Define the state update function - Specify with a function how to update the state using the previous state and the new values from an input stream.
In every batch, Spark will apply the state update function for all existing keys, regardless of whether they have new data in a batch or not. If the update function returns None then the key-value pair will be eliminated.

Let’s illustrate this with an example. Say you want to maintain a running count of each word seen in a text data stream. Here, the running count is the state and it is an integer. We define the update function as:

Let's use the same approach for WordCount problem. We need to:

    def updateFunction(newValues, runningCount):
        if runningCount is None:
            runningCount = 0
        return sum(newValues, runningCount)  # add the new values with the previous running count to get the new count


    runningCounts = pairs.updateStateByKey(updateFunction)


The update function will be called for each word, with newValues having a sequence of 1’s (from the (word, 1) pairs) and the runningCount having the previous count. For the complete Python code, take a look at the example stateful_network_wordcount.py.

Note that using updateStateByKey requires the checkpoint directory to be configured.

#### Stateful count of the sum of numbers

> A sequence of integers `0, 1, 2, 3, 4, ...` is given, coming in real time. Count its sum.

```python
import os
from time import sleep
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

sc = SparkContext(master='yarn')

NUM_BATCHES = 10  # sequence length
batches = [sc.parallelize([num]) for num in range(NUM_BATCHES)]

BATCH_TIMEOUT = 5 # RDD generation period
ssc = StreamingContext(sc, BATCH_TIMEOUT)
dstream = ssc.queueStream(rdds=batches)

def print_always(rdd):
    print("Result: {}".format(rdd.collect()[0]))

def aggregator(values, old):
    return (old or 0) + sum(values)

# `updateStateByKey` needs key-value structue so you need to specify a dummy key "res"
# and then remove it after aggregation

dstream.map(lambda num: ('res', num)) \
   .updateStateByKey(aggregator) \
   .map(lambda x: x[1]) \
   .foreachRDD(print_always)

# assign a unique name to each checkpoint
ssc.checkpoint('./checkpoint{}'.format(time.strftime("%Y_%m_%d_%H_%M_%s", time.gmtime())))  # save accumulated results
ssc.awaitTermination()  # wait for SIGINT (^C)
```

#### Stateful WordCount


* add `updateStateByKey(func)`
* alter `print_always()`

#### Enable the application to stop automatically when the data is depleted

1) Before processing, check whether the data is still incoming.

```python
finished = False

def set_ending_flag(rdd):
    global finished
    if rdd.isEmpty():
        finished = True

dstream.foreachRDD(set_ending_flag)
```

2) Instead of calling `ssc.awaitTermination()`, stop the context when the input ends.

```python
ssc.start()
while not finished:
    pass
ssc.stop()
```

#### Sources

`/home/velkerr/seminars/hobod2019/16-spark-streaming`

#### Additional examples

[Official Spark repo](https://github.com/apache/spark/tree/master/examples/src/main/python/streaming)
