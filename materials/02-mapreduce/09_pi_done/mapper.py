#! /usr/bin/env python3

import random
import sys

for line in sys.stdin:
    x, y = map(float, line.strip().split())
    is_inside = 1 if (x * x + y * y <= 1) else 0
    print("reporter:counter:Point stats,All points,{}".format(1), file=sys.stderr)
    print("reporter:counter:Point stats,Inside points,{}".format(is_inside), file=sys.stderr)
